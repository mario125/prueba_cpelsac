//routes/router.js
const { Router } = require("express");
const { createUser,
    depositarrest,
    retirar,
    obtenerSaldo,
    obtenerUltimasTransacciones,
    searchUser,
    userPassword } = require("../controllers/users.controller");
const router = Router();

// Crear usuario
router.post("/", createUser);
router.post("/search", searchUser);
router.post("/userpassword", userPassword);
router.post("/deposito", depositarrest);
router.post("/retiro", retirar);
router.post("/saldo", obtenerSaldo);
router.post("/movimientos", obtenerUltimasTransacciones);

module.exports = router;

