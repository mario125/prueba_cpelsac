require('dotenv').config();
const functions = require("firebase-functions");
const express = require("express");
const cors = require("cors");
const admin = require("firebase-admin");

const app = express();
var serviceAccount = require("./key.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});

app.use(cors({ origin: true }));
app.use(express.json()); // Asegúrate de que el servidor pueda parsear JSON

app.get("/hello", (req, res) => {
    res.status(200).json({ message: "Hello" });
});

// Importar rutas de usuarios
app.use("/api/users", require("./routes/route"));

// Exportar la app para Firebase Functions
exports.app = functions.https.onRequest(app);

// Iniciar servidor local
const PORT = process.env.PORT || 7000;
app.listen(PORT, () => {
    console.log(`Servidor HTTP está ejecutándose en el puerto ${PORT}`);
});
