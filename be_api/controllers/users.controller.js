const admin = require("firebase-admin");
const { Router } = require("express");

const db = admin.firestore();


const realizarTransaccion = async (uid, monto, tipo) => {
    try {
        const saldoRef = db.collection("saldos").doc(uid);
        await db.runTransaction(async (transaccion) => {
            const saldoDoc = await transaccion.get(saldoRef);

            if (!saldoDoc.exists) {
                throw new Error("No se encontró el saldo del usuario");
            }

            const saldoActual = saldoDoc.data().saldo;
            let nuevoSaldo;

            if (tipo === "depósito") {
                nuevoSaldo = saldoActual + parseFloat(monto);
            } else if (tipo === "retiro") {
                if (saldoActual < parseFloat(monto)) {
                    throw new Error("Fondos insuficientes");
                }
                nuevoSaldo = saldoActual - parseFloat(monto);
            }

            transaccion.update(saldoRef, { saldo: nuevoSaldo });
            await registrarTransaccion(uid, tipo, parseFloat(monto));
        });

        return { success: true, mensaje: `${tipo} realizado correctamente` };
    } catch (error) {
        throw new Error(`Error al realizar ${tipo}: ${error.message}`);
    }
};


const depositar = async (uid, monto) => {
    return realizarTransaccion(uid, monto, "depósito");
};


const depositarrest = async (req, res) => {
    const { uid, monto } = req.body;

    try {
        const resultado = await realizarTransaccion(uid, monto, "depósito");
        return res.status(200).json(resultado);
    } catch (error) {
        return res.status(500).json({ mensaje: error.message });
    }
};

// Función para crear un usuario, su saldo inicial y un depósito inicial de cero
const createUser = async (req, res) => {
    const { nombre, apellido, correo, password } = req.body;

    try {
        const usuariosRef = db.collection("usuarios");
        const querySnapshot = await usuariosRef.where('correo', '==', correo).limit(1).get();

        if (!querySnapshot.empty) {
            // Si el usuario ya existe, retornar el ID y el correo electrónico
            const usuarioExistente = querySnapshot.docs[0];
            const datosUsuario = usuarioExistente.data();
            return res.status(500).json({ correo: datosUsuario.correo, mensaje: "Usuario ya existe" });
        }

        // Crear nuevo usuario
        const nuevoUsuarioRef = usuariosRef.doc();
        await nuevoUsuarioRef.set({ nombre, apellido, correo, password });

        // Obtener el ID del usuario recién creado
        const uid = nuevoUsuarioRef.id;

        // Inicializar saldo en cero
        await crearOActualizarSaldo(uid, 0);

        // Registrar depósito inicial de cero
        await depositar(uid, 0);

        return res.status(200).json({ uid, correo });
    } catch (error) {
        return res.status(500).json({ mensaje: "Error al crear usuario", error: error.message });
    }
};

const userPassword = async (req, res) => {
    const {correo, password } = req.body;

    try {
        const usuariosRef = db.collection("usuarios");
        const querySnapshot = await usuariosRef.where('correo', '==', correo)
                                               .where('password', '==', password)
                                               .limit(1)
                                               .get();

        if (!querySnapshot.empty) {
            
            const usuarioExistente = querySnapshot.docs[0];
            const datosUsuario = usuarioExistente.data();
            return res.status(200).json({ uid: usuarioExistente.id, correo: datosUsuario.correo, mensaje: "Usuario ya existe" });
        }

        return res.status(500).json({ mensaje: "Usted no esta registrado" });
    } catch (error) {
        return res.status(500).json({ mensaje: "Error al crear usuario", error: error.message });
    }
};

const searchUser = async (req, res) => {
    const { correo } = req.body;

    try {
        const usuariosRef = db.collection("usuarios");
        const querySnapshot = await usuariosRef.where('correo', '==', correo).limit(1).get();

        if (!querySnapshot.empty) {
            
            const usuarioExistente = querySnapshot.docs[0];
            const datosUsuario = usuarioExistente.data();
            return res.status(200).json({ uid: usuarioExistente.id, correo: datosUsuario.correo, mensaje: "Usuario ya existe" });
        }

        return res.status(500).json({ mensaje: "Usted no esta registrado" });
    } catch (error) {
        return res.status(500).json({ mensaje: "Error interno", error: error.message });
    }
};

// Función para crear o actualizar el saldo de un usuario
const crearOActualizarSaldo = async (uid, saldoInicial = 0) => {
    try {
        const saldoRef = db.collection("saldos").doc(uid);
        const saldoDoc = await saldoRef.get();

        if (!saldoDoc.exists) {
            await saldoRef.set({ saldo: parseFloat(saldoInicial) });
        }
    } catch (error) {
        throw new Error("Error al crear o actualizar saldo: " + error.message);
    }
};

// Función para realizar un retiro de la cuenta de un usuario
const retirar = async (req, res) => {
    const { uid, monto } = req.body;

    try {
        const saldoRef = db.collection("saldos").doc(uid);
        await db.runTransaction(async (transaccion) => {
            const saldoDoc = await transaccion.get(saldoRef);

            if (!saldoDoc.exists) {
                throw new Error("No se encontró el saldo del usuario");
            }

            const saldoActual = saldoDoc.data().saldo;

            if (saldoActual < parseFloat(monto)) {
                throw new Error("Fondos insuficientes");
            }

            const nuevoSaldo = saldoActual - parseFloat(monto);
            transaccion.update(saldoRef, { saldo: nuevoSaldo });

            await registrarTransaccion(uid, "retiro", parseFloat(monto));
        });

        return res.status(200).json({ mensaje: "Retiro realizado correctamente" });
    } catch (error) {
        return res.status(500).json({ mensaje: "Error al realizar el retiro: " + error.message });
    }
};

// Función para registrar una transacción en la colección "transacciones"
const registrarTransaccion = async (uid, tipo, monto) => {
    try {
        const fechaActual = new Date();

        const formattedDate = `${fechaActual.getDate()}/${fechaActual.getMonth() + 1}/${fechaActual.getFullYear()} ${fechaActual.getHours()}:${fechaActual.getMinutes()}:${fechaActual.getSeconds()}`;

        await db.collection("transacciones").add({
            uid: uid,
            tipo: tipo,
            monto: parseFloat(monto),
            timestamp: admin.firestore.FieldValue.serverTimestamp(),
            fecha: formattedDate
        });
    } catch (error) {
        throw new Error("Error al registrar transacción: " + error.message);
    }
};

// Función para obtener el saldo actual de un usuario
const obtenerSaldo = async (req, res) => {

    const { uid } = req.body;
    try {
        const saldoRef = db.collection("saldos").doc(uid);
        const saldoDoc = await saldoRef.get();

        if (!saldoDoc.exists) {
            throw new Error("No se encontró el saldo del usuario");
        }


        return res.status(200).json({ estatus: "ok", saldo: parseFloat(saldoDoc.data().saldo), message: "Saldo disponible" });
    } catch (error) {
        return res.status(400).json({ estatus: "fail", saldo: 0, message: "Error al obtener el saldo: " + error.message });

    }
};

// Función para obtener los últimos 5 movimientos de un usuario
const obtenerUltimasTransacciones = async (req, res) => {
    const { uid } = req.body;
    try {
        const transaccionesRef = db.collection("transacciones")
            .where("uid", "==", uid)
            .orderBy("timestamp", "desc")
            .limit(5);

        const snapshot = await transaccionesRef.get();
        const transacciones = [];
        snapshot.forEach((doc) => {
            transacciones.push({
                id: doc.id,
                ...doc.data(),
                monto: parseFloat(doc.data().monto)
            });
        });


        return res.status(200).json({ estatus: "ok", transacciones: transacciones, message: "Operaciones ejecutadas" });
    } catch (error) {

        return res.status(400).json({ estatus: "fail", transacciones: null, message: "Error al obtener los últimos movimientos: " + error.message });
    }
};


module.exports = {
    createUser,
    depositar,
    retirar,
    obtenerSaldo,
    obtenerUltimasTransacciones,
    depositarrest,
    searchUser,
    userPassword
};
