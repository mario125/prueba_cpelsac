import Header from "../components/Header"
import Login from "../components/Login"

export default function LoginPage(){
    return(
        <>
            <Header
                heading="Ingrese a su cuenta Ninja"
                paragraph="¿Aún no tienes una cuenta? "
                linkName="Inscribirse"
                linkUrl="/signup"
                />
               <Login/>
        </>
    )
}