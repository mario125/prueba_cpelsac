import Header from "../components/Header";
import Signup from "../components/Signup";

export default function SignupPage(){
    return(
        <>
            <Header
              heading="Regístrese para crear una cuenta"
              paragraph="¿Ya tienes una cuenta? "
              linkName="Login"
              linkUrl="/"
            />
            <Signup/>
        </>
    )
}