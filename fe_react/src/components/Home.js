import React, { useState, useContext, useEffect } from 'react';
import axios from 'axios';
import Tabuser from './TabHomeUser';
import { UserContext } from '../context/UserContext';
import { rechargeFields, withdrawFields } from "../constants/formFields"; 
import Input from "./Input";
import FormAction from "./FormAction";

import { ToastContainer, toast, TypeOptions } from "react-toastify";
import "react-toastify/ReactToastify.min.css";
const types = ["success", "info", "warning", "error"];
const Home = () => {
  const [activeTab, setActiveTab] = useState('home');
  const { user } = useContext(UserContext);
  const { userId } = useContext(UserContext);
  const [saldo, setSaldo] = useState(null); 
  const [formState, setFormState] = useState({}); 
  const [movimientosList, setMovimientosList] = useState([]); 

 
  const handleChange = (e) => {
    setFormState({ ...formState, [e.target.id]: e.target.value });
  };

  
  const handleSubmit = (e) => {

    e.preventDefault();
    if (activeTab === 'recargarSaldo') {
      const { recharges } = formState;
      axios.post(process.env.REACT_APP_API_URL+'/users/deposito', { uid: userId, monto: recharges })
        .then(response => {
          toast(response.data.mensaje, {
            type: 'success'
          });
          setFormState(prevState => ({
            ...prevState,
            recharges: ''
          }));
        })
        .catch(error => {
          if (error.response.status == 500) {
            toast(error.response.data.mensaje, {
              type: 'warning'
            });
          } else {
            toast("Error no controlado", {
              type: 'warning'
            });
          }
        });
    } else if (activeTab === 'retiroSaldo') {
      const { withdrawamount } = formState;
      axios.post(process.env.REACT_APP_API_URL+'/users/retiro', { uid: userId, monto: withdrawamount })
        .then(response => {
          toast(response.data.mensaje, {
            type: 'success'
          });
          setFormState(prevState => ({
            ...prevState,
            withdrawamount: ''
          }));
        })
        .catch(error => {
          if (error.response.status == 500) {
            toast(error.response.data.mensaje, {
              type: 'warning'
            });
          } else {
            toast("Error no controlado", {
              type: 'warning'
            });
          }
        });
    }
  };


  useEffect(() => {
    if (activeTab === 'saldo') {
      axios.post(process.env.REACT_APP_API_URL+'/users/saldo', { uid: userId })
        .then(response => {
          setSaldo(response.data.saldo);
        })
        .catch(error => {
          console.error('Error fetching saldo:', error);
        });
    }
    else if (activeTab === 'movimientos') {    
      axios.post(process.env.REACT_APP_API_URL+'/users/movimientos', { uid: userId })
        .then(response => {        
          setMovimientosList(response.data.transacciones);
        })
        .catch(error => {
          if (error.response.status == 500) {
            toast(error.response.data.mensaje, {
              type: 'warning'
            });
          } else {
            toast("Error no controlado", {
              type: 'warning'
            });
          }
        });
    }
  }, [activeTab]);

 
  const renderContent = () => {
    switch (activeTab) {
      case 'home':
        return (
          <div className="items-center justify-center">
            {user && (
              <Tabuser
                url={user.imageUrl ?? 'https://ninjapos.pe/assets/cvg/logo.png'}
                last_name={user.familyName}
                email={user.email ?? user.email_address}
                name={user.name}
              />
            )}
          </div>
        );
      case 'saldo':
        return (
          <div className=" items-center justify-center">
            {saldo !== null ? (
              <div className="text-center">
                <p className="text-1xl mb-4">Saldo disponible:</p>
                <p className="font-bold text-3xl">{saldo}</p>
              </div>
            ) : (
              <p>Cargando saldo...</p>
            )}
          </div>
        );
      case 'recargarSaldo':
      case 'retiroSaldo':
        const currentFields = activeTab === 'recargarSaldo' ? rechargeFields : withdrawFields;
        return (
          <div>
            <form className="mt-8 space-y-6" onSubmit={handleSubmit}>
              <div className="-space-y-px">
                {currentFields.map(field =>
                  <Input
                    key={field.id}
                    handleChange={handleChange}
                    value={formState[field.id] || ''}
                    labelText={field.labelText}
                    labelFor={field.labelFor}
                    id={field.id}
                    name={field.name}
                    type={field.type}
                    isRequired={field.isRequired}
                    placeholder={field.placeholder}
                  />
                )}
              </div>
              <FormAction handleSubmit={handleSubmit} text={activeTab === 'recargarSaldo' ? "Recargar saldo" : "Retirar saldo"} />
            </form>
          </div>
        );
      case 'movimientos':
        return (
          <div className="items-center justify-center">
            <h2 className="text-2xl font-bold mb-4">Movimientos Recientes</h2>
            <div className="space-y-4">
              {movimientosList.map((movimiento, index) => (
                <div key={index} className="border rounded-md p-4">
                  <p><strong>Fecha:</strong> {movimiento.fecha}</p>
                  <p><strong>Tipo:</strong> {movimiento.tipo}</p>
                  <p><strong>Monto:</strong> {movimiento.monto}</p>
                </div>
              ))}
            </div>
          </div>
        );
      default:
        return null;
    }
  };

  return (
    <div>
      <div className="flex space-x-4 p-4">
        <button
          className={`px-4 py-2 rounded ${activeTab === 'home' ? 'bg-purple-700 text-white' : 'bg-gray-200'}`}
          onClick={() => setActiveTab('home')}
        >
          Home
        </button>
        <button
          className={`px-4 py-2 rounded ${activeTab === 'saldo' ? 'bg-purple-700 text-white' : 'bg-gray-200'}`}
          onClick={() => setActiveTab('saldo')}
        >
          Saldo
        </button>
        <button
          className={`px-4 py-2 rounded ${activeTab === 'recargarSaldo' ? 'bg-purple-700 text-white' : 'bg-gray-200'}`}
          onClick={() => setActiveTab('recargarSaldo')}
        >
          Recargar
        </button>
        <button
          className={`px-4 py-2 rounded ${activeTab === 'retiroSaldo' ? 'bg-purple-700 text-white' : 'bg-gray-200'}`}
          onClick={() => setActiveTab('retiroSaldo')}
        >
          Retirar
        </button>
        <button
          className={`px-4 py-2 rounded ${activeTab === 'movimientos' ? 'bg-purple-700 text-white' : 'bg-gray-200'}`}
          onClick={() => setActiveTab('movimientos')}
        >
          Movimientos
        </button>
      </div>
      <div className="p-4">
        {renderContent()}
      </div>
      <ToastContainer position="bottom-right" newestOnTop />
    </div>
  );
};

export default Home;
