import React, { useState, useEffect } from 'react';
import Input from './Input';
import FormAction from './FormAction';
import GoogleLogin from 'react-google-login';
import { gapi } from 'gapi-script';
import { signupFields } from '../constants/formFields';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { ToastContainer, toast, TypeOptions } from "react-toastify";
import "react-toastify/ReactToastify.min.css";


const fields = signupFields;
const clientID =process.env.REACT_APP_KEY_GOOGLE;

export default function Signup() {

  const navigate = useNavigate();
  const [signupState, setSignupState] = useState({
    username: '',
    email: '',
    password: '',
    'confirm-password': '',
  });

  const handleChange = (e) => {
    const { id, value } = e.target;
    setSignupState({ ...signupState, [id]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();


    if (signupState.password !== signupState['confirm-password']) {
      alert('Las contraseñas no coinciden');
      return;
    }


    createAccount();
  };

  const createAccount = () => {

    const { username, emailaddress, password } = signupState;
    console.log(signupState);
    console.log('Datos a enviar:', { username, emailaddress, password });

    axios.post(process.env.REACT_APP_API_URL+'/users/', {
      nombre: username,
      apellido: username,
      correo: emailaddress,
      password: password
    })
      .then(response => {
        toast(response.data.mensaje, {
          type: 'success'
        });
        navigate('/');

      })
      .catch(error => {
        console.log(error);
        if (error.response.status === 500) {
          toast(error.response.data.mensaje, {
            type: 'warning'
          });
        } else {
          toast("Error no controlado", {
            type: 'warning'
          });
        }
      });
  };

  const onFailure = (response) => {
    console.log('Algo salió mal', response);
  };

  const onSuccess = (response) => {

    console.log('Inicio de sesión exitoso con Google', response);

    const { name, email, givenName, familyName, googleId } = response.profileObj;



    axios.post(process.env.REACT_APP_API_URL+'/users/', {
      nombre: name,
      apellido: givenName,
      correo: email,
      password: "--"
    })
      .then(response => {
        toast(response.data.mensaje, {
          type: 'success'
        });
        navigate('/');

      })
      .catch(error => {
        console.log(error);
        if (error.response.status === 500) {
          toast(error.response.data.mensaje, {
            type: 'warning'
          });
        } else {
          toast("Error no controlado", {
            type: 'warning'
          });
        }
      });
  };

  useEffect(() => {
    function start() {
      gapi.client.init({
        clientId: clientID,
      });
    }
    gapi.load('client:auth2', start);
  }, []);

  const handleGoogleLoginClick = () => {
    gapi.auth2.getAuthInstance().signIn();
  };

  return (
    <form className="mt-8 space-y-6" onSubmit={handleSubmit}>
      <div className="">
        {fields.map((field) => (
          <Input
            key={field.id}
            handleChange={handleChange}
            value={signupState[field.id]}
            labelText={field.labelText}
            labelFor={field.labelFor}
            id={field.id}
            name={field.name}
            type={field.type}
            isRequired={field.isRequired}
            placeholder={field.placeholder}
          />
        ))}
        <FormAction handleSubmit={handleSubmit} text="Registrar" />
        <GoogleLogin
          clientId={clientID}
          onSuccess={onSuccess}
          onFailure={onFailure}
          className="group relative w-full flex items-center justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-purple-600 hover:bg-purple-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-purple-500 mt-10"
          buttonText="Registrarme con Google"
          cookiePolicy={'single_host_origin'}
          onClick={handleGoogleLoginClick}
        />
        <ToastContainer position="bottom-right" newestOnTop />
      </div>
    </form>
  );
}
