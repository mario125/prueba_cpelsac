import React, { useContext } from 'react';
import { GoogleLogout } from 'react-google-login';
import { useNavigate } from 'react-router-dom';
import { UserContext } from '../context/UserContext';

const Tabuser = ({ url, email, name, last_name }) => {
  const { setUser } = useContext(UserContext);
  const navigate = useNavigate();

  const handleSubmitLogout = () => {
    const confirmLogout = window.confirm('¿Está seguro de que desea cerrar sesión?');
    if (confirmLogout) {
      setUser(null);
     
      navigate('/');
    }
  };

  return (
    <div>
      <div className="w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
        <div className="flex flex-col items-center pb-10">
          <br />
          <img className="w-24 h-24 mb-3 rounded-full shadow-lg" src={url} alt="Profile" />
          <h5 className="mb-1 text-xl font-medium text-gray-900 dark:text-white">{name}</h5>
          <span className="text-sm text-gray-500 dark:text-gray-400">{email}</span>
          <div className="flex mt-4 md:mt-6">
            <GoogleLogout
              clientId="152246615921-plavoo3cte9rc53n7pdfn9p7lbgm2oa1.apps.googleusercontent.com"
              buttonText="Logout"
              onLogoutSuccess={handleSubmitLogout}
              className='group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-purple-600 hover:bg-purple-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-purple-500 mt-10'
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Tabuser;
