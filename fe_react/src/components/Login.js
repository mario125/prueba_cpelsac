import React, { useState, useContext, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { loginFields } from "../constants/formFields";
import FormAction from "./FormAction";
import axios from 'axios';
import FormExtra from "./FormExtra";
import Input from "./Input";
import JSONPretty from 'react-json-pretty';
import GoogleLogin from 'react-google-login';
import { gapi } from 'gapi-script';
import { UserContext } from '../context/UserContext';
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/ReactToastify.min.css";


const fields = loginFields;
let fieldsState = {};
fields.forEach(field => fieldsState[field.id] = '');

const clientID = process.env.REACT_APP_KEY_GOOGLE;

export default function Login() {
    const navigate = useNavigate();
    const { user, setUser, userId, setUserId } = useContext(UserContext);

    const [loginState, setLoginState] = useState(fieldsState);

    const handleChange = (e) => {
        setLoginState({ ...loginState, [e.target.id]: e.target.value })
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        authenticateUser();
    }

    const authenticateUser = () => {
        const { email_address, password } = loginState;
        setUser(loginState);
        axios.post(process.env.REACT_APP_API_URL + '/users/userPassword', {
            correo: email_address,
            password: password
        })
            .then(response => {
                toast(response.data.mensaje, {
                    type: 'success'
                });
                setUserId(response.data.uid);
                navigate('/home');
            })
            .catch(error => {
               
                if (error.response.status == 500) {
                    toast(error.response.data.mensaje, {
                        type: 'warning'
                    });
                } else {
                    toast("Error no controlado", {
                        type: 'warning'
                    });
                }
            });
    }

    const onSuccess = (response) => {
        const { name, email, givenName, familyName, googleId } = response.profileObj;

        setUser(response.profileObj);

        axios.post(process.env.REACT_APP_API_URL + '/users/search', {
            correo: email
        })
            .then(response => {
                toast(response.data.mensaje, {
                    type: 'success'
                });
                setUserId(response.data.uid);
                navigate('/home');
            })
            .catch(error => {
                if (error.response.status === 500) {
                    toast(error.response.data.mensaje, {
                        type: 'warning'
                    });
                } else {
                    toast("Error no controlado", {
                        type: 'warning'
                    });
                }
            });
    }

    const onFailure = (response) => {
        console.log("Something went wrong", response);
    }

    useEffect(() => {
        function start() {
            gapi.client.init({
                clientId: clientID,
            });
        }
        gapi.load("client:auth2", start);
    }, []);

    return (
        <form className="mt-8 space-y-6" onSubmit={handleSubmit}>

            <div className="-space-y-px">
                {fields.map(field =>
                    <Input
                        key={field.id}
                        handleChange={handleChange}
                        value={loginState[field.id]}
                        labelText={field.labelText}
                        labelFor={field.labelFor}
                        id={field.id}
                        name={field.name}
                        type={field.type}
                        isRequired={field.isRequired}
                        placeholder={field.placeholder}
                    />
                )}
            </div>
            <FormExtra />
            <FormAction handleSubmit={handleSubmit} text="Login" />
            <GoogleLogin
                clientId={clientID}
                onSuccess={onSuccess}
                onFailure={onFailure}
                className='group relative w-full flex items-center justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-purple-600 hover:bg-purple-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-purple-500 mt-10'
                buttonText="Continuar con Google"
                cookiePolicy={"single_host_origin"}
                isSignedIn={true}
            />

            <ToastContainer position="bottom-right" newestOnTop />

            {/* <JSONPretty data={user} />  */}
        </form>
    )
}
