const rechargeFields = [
    {
        labelText: "recharge",
        labelFor: "recharge",
        id: "recharges",
        name: "recharge",
        type: "number",
        step: "any",
        autoComplete: "off",
        isRequired: true,
        placeholder: "Ingrese el monto a recargar"
    }
];
const withdrawFields = [
    {
        labelText: "Monto a retirar",
        labelFor: "withdrawamount",
        id: "withdrawamount",
        name: "withdrawAmount",
        type: "number",
        autoComplete: "off",
        isRequired: true,
        placeholder: "Ingrese el monto a retirar"
    }
];
const loginFields = [
    {
        labelText: "Email address",
        labelFor: "email-address",
        id: "email_address",
        name: "email",
        type: "email",
        autoComplete: "email",
        isRequired: true,
        placeholder: "Direción de correo electrónico"
    },
    {
        labelText: "Password",
        labelFor: "password",
        id: "password",
        name: "password",
        type: "password",
        autoComplete: "current-password",
        isRequired: true,
        placeholder: "Contraseña"
    }
]

const signupFields = [
    {
        labelText: "Username",
        labelFor: "username",
        id: "username",
        name: "username",
        type: "text",
        autoComplete: "username",
        isRequired: true,
        placeholder: "name"
    },
    {
        labelText: "Email address",
        labelFor: "emailaddress",
        id: "emailaddress",
        name: "email",
        type: "email",
        autoComplete: "email",
        isRequired: true,
        placeholder: "email"
    },
    {
        labelText: "Password",
        labelFor: "password",
        id: "password",
        name: "password",
        type: "password",
        autoComplete: "current-password",
        isRequired: true,
        placeholder: "password"
    },
    {
        labelText: "Confirm Password",
        labelFor: "confirm-password",
        id: "confirm-password",
        name: "confirm-password",
        type: "password",
        autoComplete: "confirm-password",
        isRequired: true,
        placeholder: "Confirm password"
    }
]

export { loginFields, signupFields, rechargeFields, withdrawFields }