import React, { useContext } from 'react';
import {
  BrowserRouter,
  Routes,
  Route,
  Navigate
} from "react-router-dom";
import SignupPage from './pages/Signup';
import LoginPage from './pages/Login';
import HomePage from './pages/Home';
import NotFoundPage from './pages/NotFound'; 
import { UserContext } from './context/UserContext'; 


function App() {
  const { user } = useContext(UserContext);

  return (
    <div className="min-h-full h-screen flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
      <div className="max-w-md w-full space-y-8">
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<LoginPage />} />
            <Route path="/signup" element={<SignupPage />} />
            {/* Validar si el usuario está autenticado antes de acceder a /home */}
            <Route
              path="/home"
              element={user ? <HomePage /> : <Navigate to="/" />}
            />
            {/* Ruta para manejar cualquier otra ruta no definida */}
            <Route path="*" element={<NotFoundPage />} />
          </Routes>
        </BrowserRouter>
      </div>
    </div>
  );
}

export default App;
